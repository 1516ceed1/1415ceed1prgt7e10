
import javax.swing.JFrame;
import javax.swing.JLabel;
/*
 Aplicación simple con una etiqueta y sin JPanel
 */

public class MiJLabel1 extends JFrame {

    private JLabel jl;

    MiJLabel1() {
        initComponents();
    }

    private void initComponents() {
        jl = new JLabel();
        add(jl);
        jl.setText("Hola Mundo");
        setVisible(true);
    }

    public static void main(String args[]) {
        new MiJLabel1();
    }

}
