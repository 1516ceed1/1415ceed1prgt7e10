import javax.swing.*;
import java.awt.*; // JScrollBar, setLayout

public class Componentes  {

  public static void main(String[] args ) {
    JLabel jlabel = new JLabel("JLabel");
    JButton jbutton = new JButton("JButton");
    JTextField jtextfield = new JTextField("JTextField");
    JTextArea jtextarea = new JTextArea("JTextArea");
    JCheckBox  jcheckbox = new JCheckBox("JCheckBox");
    JRadioButton jradiobutton = new JRadioButton("JRadioButton");
    String[] opciones = { "Opcion1", "Opcion2", "Opcion3"};
    JComboBox jcombobox = new JComboBox(opciones);
    JScrollBar jscrollbar = new JScrollBar(Scrollbar.HORIZONTAL,0,64,0,255);
    try {
      JFrame frame = new JFrame("Frame");
      frame.getContentPane().add(jlabel);
      frame.getContentPane().add(jbutton);
      frame.getContentPane().add(jtextfield);
      frame.getContentPane().add(jtextarea);
      frame.getContentPane().add(jcheckbox);
      frame.getContentPane().add(jradiobutton);
      frame.getContentPane().add(jcombobox);
      frame.getContentPane().add(jscrollbar);
      frame.setLayout( new GridLayout(0,1) );
      frame.pack();
      frame.setVisible(true);
    } catch (Exception e) {}
  }
}
