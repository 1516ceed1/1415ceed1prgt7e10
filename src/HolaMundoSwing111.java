
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HolaMundoSwing111 extends JFrame {

    private JLabel label;

    public HolaMundoSwing111() {
        initComponents();
    }

    public void initComponents() {
        setTitle("HolaMundoSwing11");
        label = new JLabel("Hola Mundo");
        add(label);

        setSize(500, 200); // Tamaño de la ventana
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        setVisible(true); // Ver el JFrame

    }

    public static void main(String[] args) {
        new HolaMundoSwing11();
    }
}
