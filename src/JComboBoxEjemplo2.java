
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

public class JComboBoxEjemplo2 {

    private JLabel jl;
    private JComboBox jc;
    private JFrame jf;

    public JComboBoxEjemplo2() {
        // Creacion del JTextField
        jl = new JLabel("             ");
        ArrayList items = new ArrayList();
        items.add("uno");
        items.add("dos");
        items.add("tres");

        // Creacion del JComboBox y anyadir los items mediante arraylist
        jc = new JComboBox();
        for (int i = 0; i < items.size(); i++) {
            jc.addItem(items.get(i));
            if (items.get(i).equals("dos")) {
                jc.setSelectedItem(items.get(i));
            }
        }

        // Accion a realizar cuando el JComboBox cambia de item seleccionado.
        jc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jl.setText(jc.getSelectedItem().toString());
            }

        });

        // Creacion de la ventana con los componentes
        jf = new JFrame();
        jf.setLayout(new FlowLayout());
        jf.add(jc);
        jf.add(jl);
        jf.pack();
        jf.setLocationRelativeTo(null); // Centrar
        jf.setVisible(true);
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JComboBoxEjemplo2();
    }

}
