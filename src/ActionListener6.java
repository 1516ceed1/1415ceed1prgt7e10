
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ActionListener6 extends JFrame implements ActionListener {

    private JButton b1;
    private JButton b2;

    public ActionListener6() {
        super("ActionListener6");

        setLayout(new FlowLayout());

        b1 = new JButton("Pulsame");
        b1.addActionListener(this);
        b1.setActionCommand("Pulsame");
        add(b1);

        b2 = new JButton("Salir");
        b2.addActionListener(this);
        b2.setActionCommand("Salir");
        add(b2);

        pack();
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
    }

    private void metodo1() {
        b1.setText("Pulsado");
    }

    private void metodo2() {
        System.exit(0); // Cierra la aplicación
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();

        if (command.equals("Pulsame")) {
            metodo1();
        } else if (command.equals("Salir")) {
            metodo2();
        }

    }

    public static void main(String[] argc) {
        new ActionListener6().setVisible(true);
    }

}
