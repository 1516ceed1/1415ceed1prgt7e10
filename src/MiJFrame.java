
import javax.swing.JFrame;
import javax.swing.JLabel;
/*
 Aplicación simple con una etiqueta y sin JPanel
 */

public class MiJFrame extends JFrame {

    private JLabel jl;

    public MiJFrame() {
        jl = new JLabel();
        add(jl);
        jl.setText("Hola Mundo");

        setSize(200, 200);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE); // Cierra Aplicacion
        setLocationRelativeTo(null); // Centrar
    }

    public static void main(String args[]) {
        new MiJFrame();
    }

}
