
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MiJTextField1 {

    private JFrame f;
    private JPanel p;
    private JTextField t;
    private JLabel l;

    MiJTextField1() {

        f = new JFrame();
        p = new JPanel();
        l = new JLabel();
        t = new JTextField();

        f.add(p);
        p.add(l);
        p.add(t);

        l.setText("Nombre: ");
        t.setText("Pepe");
        t.setColumns(15);
        p.setBackground(Color.yellow);

        f.setLayout(new FlowLayout());
        p.setLayout(new FlowLayout());

        f.setLocationRelativeTo(null); // Centrar
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        f.setSize(250, 100);
        f.setVisible(true);
        //f.pack();
    }

    public static void main(String args[]) {
        new MiJTextField1();
    }

}
