/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt7e10.swing06bFormulario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Fichero: Fichero.java
 *
 * @date 03-abr-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class VistaFichero {

    final String FICHERO = "swing06bFormulario.csv";

    ModeloCliente primero() {

        File fs = new File(FICHERO);
        ModeloCliente cliente = null;

        if (fs.exists()) {
            try {
                StringTokenizer stringTokenizer;
                FileReader fr = new FileReader(fs);
                BufferedReader br = new BufferedReader(fr);
                String linea;

                linea = br.readLine();
                if (linea != null) {
                    stringTokenizer = new StringTokenizer(linea, ";");

                    String id;
                    String nombre;

                    id = stringTokenizer.nextToken();
                    nombre = stringTokenizer.nextToken();

                    cliente = new ModeloCliente(id, nombre);
                }

                if (fr != null) {
                    fr.close();
                }

            } catch (IOException e) {
                System.out.println("Error en fichero " + FICHERO + " " + e.getMessage());
            }
        } // if
        else {
            System.out.println("No existe el fichero " + FICHERO);
        }
        return cliente;
    }

    ModeloCliente getCliente(String id_) {

        File fs = new File(FICHERO);
        ModeloCliente cliente = null;

        if (fs.exists()) {
            try {
                StringTokenizer stringTokenizer;
                FileReader fr = new FileReader(fs);
                BufferedReader br = new BufferedReader(fr);
                String linea;

                while ((linea = br.readLine()) != null) {
                    stringTokenizer = new StringTokenizer(linea, ";");

                    String id;
                    String nombre;

                    id = stringTokenizer.nextToken();
                    nombre = stringTokenizer.nextToken();

                    cliente = new ModeloCliente(id, nombre);
                    if (id.equals(id_)) {
                        return cliente;
                    }

                } // while

                if (fr != null) {
                    fr.close();
                }

            } catch (IOException e) {
            }
        } // if
        return cliente;
    }
}
