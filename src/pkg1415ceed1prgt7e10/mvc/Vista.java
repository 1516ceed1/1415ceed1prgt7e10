/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt7e10.mvc;

/**
 * Fichero: Vista.java
 *
 * @date 16-feb-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public interface Vista {

    void setControlador(Controlador c);

    void arranca();
    // comienza la visualización

    double getCantidad();
// cantidad a convertir

    void escribeCambio(String s); //texto con la conversión
    // Constantes que definen las posibles operaciones:
    static final String AEUROS = "Pesetas a Euros";
    static final String APESETAS = "Euros a Pesetas";
}
