/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing17DefaultTableModelMVC;

/**
 * Fichero: Main.java
 * @date 09-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Main {
 
    public static void main(String[] args) {
        
        Modelo      model      = new Modelo();
        Vista       view       = new Vista(model);
        Controlador controller = new Controlador(model, view);
        
        
    }
}

