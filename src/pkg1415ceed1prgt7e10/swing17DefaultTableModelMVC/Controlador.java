/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing17DefaultTableModelMVC;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Fichero: Controlador.java
 * @date 09-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Controlador 
{
    private Modelo modelo;
    private Vista  vista;
    
    Controlador(Modelo model, Vista view) {
        modelo = model;
        vista  = view;
        view.addClearListener(new ClearListener() {});
        view.setVisible(true);
    }

    abstract class ClearListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            modelo.setModelo();
	    vista.setTabla();           			
        }
    }
}