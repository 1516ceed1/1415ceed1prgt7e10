/*
 * ColorRenderer.java
 *
 * Created on 1 de octubre de 2007, 1:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package swing12TablaColor;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author aaparicio
 */
public class ColorRenderer extends JLabel implements TableCellRenderer {

  /**
   * Creates a new instance of ColorRenderer
   */
  public ColorRenderer() {
  }

  /**
   * Devuelve un componente para personalizar la celda
   *
   * @param table Componente JTable padre
   * @param value Valor de la celda
   * @param isSelected True si la celda está seleccionada
   * @param hasFocus Si tiene el foco
   * @param row Indice de la fila
   * @param column Indice de la columna
   * @return Componente para ser visualizado
   */
  public Component getTableCellRendererComponent(JTable table, Object value,
          boolean isSelected, boolean hasFocus, int row, int column) {
    if (!isSelected) {
      this.setBackground(Color.YELLOW);
      this.setForeground(Color.PINK);

    } else {
      this.setBackground(Color.LIGHT_GRAY);
      this.setForeground(Color.RED);

    }
    this.setText((String) value);
    this.setToolTipText("El valor de esta celda es: " + value);
    this.setOpaque(true);
    return this;
  }
}
