/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Fichero: ShowInputDialogComboxBoxExample.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-mar-2014
 * http://alvinalexander.com/java/joptionpane-showinputdialog-examples
 */
public class ShowInputDialogComboxBoxExample {

  public static final String[] pizzas = {"Cheese", "Pepperoni", "Sausage", "Veggie"};

  public static void main(String[] args) {
    JFrame frame = new JFrame("Input Dialog Example 3");
    String favoritePizza = (String) JOptionPane.showInputDialog(frame,
            "What is your favorite pizza?",
            "Favorite Pizza",
            JOptionPane.QUESTION_MESSAGE,
            null,
            pizzas,
            pizzas[0]);

    // favoritePizza will be null if the user clicks Cancel
    System.out.printf("Favorite pizza is %s.\n", favoritePizza);
    System.exit(0);
  }
}
